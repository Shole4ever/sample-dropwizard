compile:
mvn clean package

run fat jar created in target folder
java -jar samsple-dropwizard-0.1-SNAPSHOT.jar 

to run in debug mode:
java -jar -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 samsple-dropwizard-0.1-SNAPSHOT.jar


Payloads for post APIs

user payload:
    {
    	"userId":"shubham.velocity",
    	"name":"Shubham Maheshwari",
    	"email":"shubham.velocity@gmail.com",
    	"mobile":"9620506569"
    }

listing payload:
    {
    	"id":"listing1",
    	"title":"Title",
    	"price":3.12
    }

update email payload:
    {
        "shubham.mah@gmail.com"
    }

APIs list:

    GET     /listing/allListings	-> list all listings for all userIDs currently stored
    GET     /listing/user/{userId} 	-> get list of all listings for a userID
    POST    /listing/{userId} 		-> save a listing for a userID
    GET     /user/allUsers		-> list all user details currently stored
    GET     /user/{userID}		-> get user details for a userID
    POST    /user			-> save user details
    POST    /user/delete/{userId} 	-> delete user details for a userID
    POST    /user/update/{userID}	-> update email of the user for the given userID


Curl commands:

-> save a user:
    curl -X POST -H "Content-Type: application/json" -d '{
        "userId":"shubham.velocity",
        "name":"Shubham Maheshwari",
        "email":"shubham.velocity@gmail.com",
        "mobile":"9620506569"
    }' 'http://localhost:8080/user'

    curl -X POST -H "Content-Type: application/json" -d '{
        "userId":"ashutosh.mehta",
        "name":"Ashutosh Mehta",
        "email":"ashu.mehta@hotmail.com",
        "mobile":"9620984532"
    }' 'http://localhost:8080/user'


-> Add listings:
    curl -X POST -H "Content-Type: application/json" -d '{
        "id":"listing1",
        "title":"Title for first listing",
        "price":3.12
    }' 'http://localhost:8080/listing/shubham.velocity'

    curl -X POST -H "Content-Type: application/json" -d '{
        "id":"listing2",
        "title":"Title for second listing",
        "price":6.24
    }' 'http://localhost:8080/listing/shubham.velocity'

    curl -X POST -H "Content-Type: application/json" -d '{
        "id":"listing1",
        "title":"Title for first listing of Ashu",
        "price":9.7
    }' 'http://localhost:8080/listing/ashutosh.mehta'


    http://localhost:8080/user/allUsers
    http://localhost:8080/user/shubham.velocity
    http://localhost:8080/listing/allListings
    http://localhost:8080/listing/user/shubham.velocity


update email:
    curl -X POST -H "Content-Type: application/json" -d 'ashutosh.mehta@gmail.com' 'http://localhost:8080/user/update/ashutosh.mehta'

