package com.shole4ever.sampleapp;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.shole4ever.sampleapp.configuration.SampleDropwizardAppConfiguration;
import com.shole4ever.sampleapp.resources.ListingResource;
import com.shole4ever.sampleapp.resources.UserResource;
import io.dropwizard.Application;
import io.dropwizard.configuration.UrlConfigurationSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by shubham.maheshwari on 08/07/15.
 */
public class SampleDropwizardService extends Application<SampleDropwizardAppConfiguration> {
    public static void main(String[] args) throws Exception {
        new SampleDropwizardService().run(new String[] {"server", "configuration.yaml"});
    }

    @Override
    public void initialize(Bootstrap<SampleDropwizardAppConfiguration> bootstrap) {
        bootstrap.setConfigurationSourceProvider(new UrlConfigurationSourceProvider() {
            @Override
            public InputStream open(String path) throws IOException {
                return ClassLoader.getSystemResourceAsStream(path);
            }
        });
        super.initialize(bootstrap);
    }

    @Override
    public void run(SampleDropwizardAppConfiguration sampleDropwizardAppConfiguration, Environment environment) throws Exception {
        Injector injector = getGuiceInjector();
        environment.jersey().register(injector.getInstance(UserResource.class));
        environment.jersey().register(injector.getInstance(ListingResource.class));
    }

    public Injector getGuiceInjector() {
        return Guice.createInjector(new SampleAppServiceModule());
    }
}
