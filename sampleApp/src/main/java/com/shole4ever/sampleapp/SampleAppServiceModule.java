package com.shole4ever.sampleapp;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.shole4ever.sampleapp.dal.api.ListingDAO;
import com.shole4ever.sampleapp.dal.api.UserDAO;
import com.shole4ever.sampleapp.dal.inmemory.InMemoryListingDao;
import com.shole4ever.sampleapp.dal.inmemory.InMemoryUserDao;

/**
 * Created by shubham.maheshwari on 08/07/15.
 */
public class SampleAppServiceModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(UserDAO.class).annotatedWith(Names.named("InMemoryUserDAO")).to(InMemoryUserDao.class);
        bind(ListingDAO.class).annotatedWith(Names.named("InMemoryListingDAO")).to(InMemoryListingDao.class);
    }
}
