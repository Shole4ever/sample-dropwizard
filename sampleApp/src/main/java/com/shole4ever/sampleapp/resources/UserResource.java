package com.shole4ever.sampleapp.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.shole4ever.sampleapp.helpers.UserResourceHelper;
import com.shole4ever.sampleapp.models.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by shubham.maheshwari on 08/07/15.
 */
@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    @Inject private UserResourceHelper helper;

    @GET
    @Timed
    @Path("/{userID}")
    public User getUserById(@PathParam("userID") String userID) throws Exception {
        return helper.getUserById(userID);
    }

    @POST
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addUser(User user) {
        helper.putUser(user);

        return Response.ok().build();
    }

    @POST
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/delete/{userId}")
    public Response deleteUser(@PathParam("userId") String userId) {
        helper.removeUser(userId);

        return Response.ok().build();
    }

    @GET
    @Timed
    @Path("/allUsers")
    public List<User> getAllUsers() {
        return helper.getAllUsers();
    }

    @POST
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/update/{userID}")
    public Response updateEmail(@PathParam("userID") String userID, String updatedEmail) {
        helper.updateEmail(userID, updatedEmail);

        return Response.ok().build();
    }
}
