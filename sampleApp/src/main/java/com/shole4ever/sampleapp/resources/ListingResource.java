package com.shole4ever.sampleapp.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.shole4ever.sampleapp.helpers.ListingResourceHelper;
import com.shole4ever.sampleapp.models.Listing;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by shubham.maheshwari on 08/07/15.
 */
@Path("/listing")
@Produces(MediaType.APPLICATION_JSON)
public class ListingResource {

    @Inject private ListingResourceHelper helper;

    @GET
    @Path("/user/{userId}")
    @Timed
    public List<Listing> getListingsForUserId(@PathParam("userId") String userId) throws Exception {
        List<Listing> listings = helper.getListingsForUserId(userId);
        if(listings == null) {
            listings = new ArrayList<>();
        }

        return listings;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Timed
    @Path("/{userId}")
    public Response saveListingForUser(Listing listing, @PathParam("userId") String userId) throws Exception {
        helper.saveListingForUser(userId, listing);

        return Response.ok().build();
    }

    @GET
    @Timed
    @Path("/allListings")
    public Map<String, List<Listing>> getAllListings() {
        return helper.getAllListings();
    }
}
