package com.shole4ever.sampleapp.dal.api;

import com.shole4ever.sampleapp.models.Listing;
import com.shole4ever.sampleapp.models.User;

import java.util.List;
import java.util.Map;

/**
 * Created by shubham.maheshwari on 08/07/15.
 */
public interface ListingDAO {
    public List<Listing> getAllListingsOfUser(User user);

    public void addListingForUser(User user, Listing listing);

    Map<String,List<Listing>> getAllListings();
}
