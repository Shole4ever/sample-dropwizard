package com.shole4ever.sampleapp.dal.inmemory;

import com.google.inject.Singleton;
import com.shole4ever.sampleapp.dal.api.UserDAO;
import com.shole4ever.sampleapp.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by shubham.maheshwari on 08/07/15.
 */
@Singleton
public class InMemoryUserDao implements UserDAO {

    private ConcurrentHashMap<String, User> userMap;

    public static final Logger logger = LoggerFactory.getLogger(InMemoryUserDao.class);

    public InMemoryUserDao() {
        this.userMap = new ConcurrentHashMap<>();
    }

    @Override
    public User getUserById(String userId) {
        if(userMap != null) {
            return userMap.get(userId);
        }

        return null;
    }

    @Override
    public void addUser(User user) {
        userMap.put(user.getUserId(), user);
    }

    @Override
    public void deleteUser(String userId) {
        if(userMap != null) {
            userMap.remove(userId);
        }
    }

    @Override
    public List<User> getAllUsers() {
        return new ArrayList<>(userMap.values());
    }
}
