package com.shole4ever.sampleapp.dal.api;

import com.shole4ever.sampleapp.models.User;

import java.util.List;

/**
 * Created by shubham.maheshwari on 08/07/15.
 */
public interface UserDAO {
    public User getUserById(String userId);

    public void addUser(User user);

    public void deleteUser(String userId);

    List<User> getAllUsers();
}
