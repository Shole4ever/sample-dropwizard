package com.shole4ever.sampleapp.dal.inmemory;

import com.google.inject.Singleton;
import com.shole4ever.sampleapp.dal.api.ListingDAO;
import com.shole4ever.sampleapp.models.Listing;
import com.shole4ever.sampleapp.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by shubham.maheshwari on 08/07/15.
 */
@Singleton
public class InMemoryListingDao implements ListingDAO {

    private Map<User, List<Listing>> listingsMap;

    public static final Logger logger = LoggerFactory.getLogger(InMemoryListingDao.class);

    public InMemoryListingDao() {
        this.listingsMap = Collections.synchronizedMap(new HashMap<User, List<Listing>>(4096));
    }

    @Override
    public List<Listing> getAllListingsOfUser(User user) {
        if(listingsMap != null) {
            return listingsMap.get(user);
        }

        return null;
    }

    @Override
    public void addListingForUser(User user, Listing listing) {
        if(listingsMap.get(user) != null) {
            logger.info("existing listings found for this user. Appending to it.");
            listingsMap.get(user).add(listing);
        } else {
            logger.info("No existing listing found for this user.");
            List<Listing> listingList = new ArrayList<>();
            listingList.add(listing);
            listingsMap.put(user, listingList);
        }
    }

    @Override
    public Map<String, List<Listing>> getAllListings() {
        Map<String, List<Listing>> listings = new HashMap<>();

        for(Map.Entry<User, List<Listing>> userListingEntry : listingsMap.entrySet()) {
            listings.put(userListingEntry.getKey().getUserId(), userListingEntry.getValue());
        }

        return listings;
    }
}
