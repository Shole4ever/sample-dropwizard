package com.shole4ever.sampleapp.helpers;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.shole4ever.sampleapp.dal.api.UserDAO;
import com.shole4ever.sampleapp.models.User;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by shubham.maheshwari on 08/07/15.
 */
public class UserResourceHelper {

    @Named("InMemoryUserDAO")
    @Inject
    private UserDAO userDAO;

    public static final Logger logger = LoggerFactory.getLogger(UserResourceHelper.class);

    public User getUserById(String userID) throws Exception {
        User user = userDAO.getUserById(userID);

        if(user == null) {
            throw new Exception("No user found");
        }
        logger.info("Found user: " + user);

        return user;
    }

    public void putUser(User user) {
        userDAO.addUser(user);
        logger.info("Added user with ID: " + user.getUserId());
    }

    public void removeUser(String userId) {
        userDAO.deleteUser(userId);
        logger.info("Removed Used with ID: " + userId);
    }

    public void updateEmail(String userId, String updatedEmail) {
        if(StringUtils.isEmpty(userId)) {
            logger.error("Null or empty userId passed");
            return;
        }

        User user = userDAO.getUserById(userId);
        if(user == null) {
            logger.error("No user present with ID: " + userId);
        }

        user.setEmail(updatedEmail);
        userDAO.addUser(user);
    }

    public List<User> getAllUsers() {
        return userDAO.getAllUsers();
    }
}
