package com.shole4ever.sampleapp.helpers;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.shole4ever.sampleapp.dal.api.ListingDAO;
import com.shole4ever.sampleapp.dal.api.UserDAO;
import com.shole4ever.sampleapp.models.Listing;
import com.shole4ever.sampleapp.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * Created by shubham.maheshwari on 08/07/15.
 */
public class ListingResourceHelper {

    @Named("InMemoryListingDAO")
    @Inject
    private ListingDAO listingDAO;

    @Named("InMemoryUserDAO")
    @Inject
    private UserDAO userDAO;

    public static final Logger logger = LoggerFactory.getLogger(ListingResourceHelper.class);

    public List<Listing> getListingsForUserId(String userId) throws Exception {
        User user = userDAO.getUserById(userId);

        if(user == null) {
            logger.error("User not found.");
            throw new Exception("User not found for ID: " + userId);
        }

        return listingDAO.getAllListingsOfUser(user);
    }

    public void saveListingForUser(String userId, Listing listing) throws Exception {
        User user = userDAO.getUserById(userId);

        if(user == null) {
            logger.error("User not found.");
            throw new Exception("User not found for ID: " + userId);
        }

        listingDAO.addListingForUser(user, listing);
    }

    public Map<String, List<Listing>> getAllListings() {
        return listingDAO.getAllListings();
    }
}
